package examples

import "gitlab.com/determinant/mxpassfile"

func Usage_Example(path string) {

	pf, err := mxpassfile.ReadPassfile(path)
	if err != nil {
		panic(err)
	}

	// token/password if found or an empty string
	token := pf.FindPassword("synapsehost or *", "localpart or *", "domain or *")
	println(token)

	// token/password if found or an empty string, using additional tags
	token = pf.FindPasswordTags("synapsehost or *", "localpart or *", "domain or *", []string{"all", "tags", "must", "fit"})
	println(token)

	// valid missuse: tags only
	token = pf.FindPasswordTagsOnly("exploit", "the", "system")
	println(token)

}
